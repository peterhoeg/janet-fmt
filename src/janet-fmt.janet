(import spork)

(defn main [& args]
  (let (pname "janet-fmt")
    (unless (compare= (length args) 2)
      (print (string pname ": No file specified"))
      (os/exit 1))
    (let (file (get args 1))
      (if (os/stat file)
        (fmt/format-file file)
        (print (string pname ": File not found, " file))))))
