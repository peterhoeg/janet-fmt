(declare-project
  :name "janet-fmt"
  :description "Janet formatter"
  :dependencies ["https://github.com/janet-lang/spork.git"])

(declare-executable
  :name "janet-fmt"
  :entry "src/janet-fmt.janet")
