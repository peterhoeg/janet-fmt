{
  description = "janet-fmt flake";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";

      lib = nixpkgs.lib.extend
        (final: prev: { });

      pkgs = import nixpkgs {
        inherit lib system;
      };

      specFile =
        let
          base = ref: name: attrs:
            lib.recursiveUpdate
              {
                enabled = 1;
                type = 1;
                hidden = false;
                description = "janet-fmt - ${ref}";
                checkinterval = 600;
                schedulingshares = 1;
                enableemail = true;
                emailoverride = "";
                keepnr = 3;
                flake = "git+ssh://git@gitlab.com/peterhoeg/janet-fmt.git?ref=${ref}";
              }
              attrs;
        in
        (pkgs.formats.json { }).generate "hydra.json" {
          main = base "refs/heads/main" "main" { };
        };
    in
    {
      packages.${system} = rec {
        janet-fmt = pkgs.callPackage ./package.nix { };
        default = janet-fmt;
      };

      devShells.${system}.default = pkgs.mkShell (
        let
          drv = self.packages.${system}.janet-fmt;
        in
        {
          inherit (drv)
            JANET_BINPATH JANET_MODPATH JANET_LIBPATH;

          shellHook = ''
            install -Dm644 ${specFile} $(git rev-parse --show-toplevel)/.ci/${specFile.name}
          '';

          nativeBuildInputs = drv.nativeBuildInputs ++ (with pkgs; [ ]);
        }
      );

      overlays.default = final: prev: {
        inherit (self.packages.${system}) janet-fmt;
      };

      hydraJobs = {
        build = self.packages.${system}.janet-fmt;
      };
    };
}
