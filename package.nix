{ stdenv, lib, janet, jpm }:

stdenv.mkDerivation rec {
  pname = "janet-fmt";
  version = "0.0.3";

  src = ./.;

  nativeBuildInputs = [ janet jpm ];

  dontConfigure = true;

  JANET_BINPATH = "./bin";
  JANET_MODPATH = "./lib";
  JANET_LIBPATH = "${lib.getLib janet}/lib";

  buildPhase = ''
    runHook preBuild

    jpm build

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

    install -Dm555 -t $out/bin build/janet-fmt

    runHook postInstall
  '';

  meta = with lib; {
    description = "Janet formatter";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ peterhoeg ];
  };
}
